/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vistas;

import java.util.List;
import java.util.Scanner;
import objetos.Producto;

/**
 *
 * @author oscar
 */
public class Vista
{
    /**
     * Muestra la cabecera de inicio del programa.
     */
    public static void mostrarCabeceraPrincipal()
    {
        System.out.println("\t\t*****************************************");
        System.out.println("\t\t*   BIENVENIDO A LA CAJA REGISTRADORA   *");
        System.out.println("\t\t*****************************************");
    }        
    
    /**
     * Muestra las opciones del menu principal.
     */
    public static void mostrarMenuPrincipal()
    {
        System.out.println("\t\t\t 1. Comprar");
        System.out.println("\t\t\t 2. Vender");
        System.out.println("\t\t\t 3. Inventario");
        System.out.println("\t\t\t 4. Compras");
        System.out.println("\t\t\t 5. Ventas");
        System.out.println("\t\t\t 6. Salir");
    }        
    
    /**
     * Muestra un cartel que indica que ingrese un numero entero para las opciones:
     */
    public static void mostrarObtenerOpcion()
    {
        System.out.print("\t\tDigite una opcion: ");
    }  
    
    /**
     * Muestra un cartel que indica que ingrese un numero entero para la cantidad de un articulo:
     */    
    public static void mostrarObtenerCantidad()
    {
        System.out.print("\t\tDigite la cantidad: ");
    } 
    
    /**
     * Muestra un cartel que indica que ingrese un numero decimal para el precio de un articulo:
     */
    public static void mostrarObtenerPrecio()
    {
        System.out.print("\t\tDigite el precio: ");
    } 
    
    /**
     * Ingreso de un numero entero por teclado para las opciones.
     * @return 
     */
    public static Integer obtenerOpcionPorTeclado()
    {
        Scanner teclado = new Scanner(System.in);
        return teclado.nextInt();
    }

    /**
     * Ingreso de un numero entero por teclado para la cantidad. 
     * @return 
     */    
    public static Integer obtenerCantidadPorTeclado()
    {
        Scanner teclado = new Scanner(System.in);
        return teclado.nextInt();
    }
    
     /**
     * Ingreso de un numero decimal por teclado para el precio.
     * @return 
     */
    public static Double obtenerPrecioPorTeclado()
    {
        Scanner teclado = new Scanner(System.in);
        return teclado.nextDouble();
    }
    
    /**
     * Muestra el menu de cabecera de compras con los articulos.
     */
    public static void mostrarMenuDeCabeceraDeCompras()
    {
        System.out.println("\t\t*****************************************");
        System.out.println("\t\t*            MENU DE COMPRAS            *");
        System.out.println("\t\t*****************************************");     
        mostrarMenuDeArticulos();
    }
    
    /**
     * Muestra el menu de cabecera de ventas con los articulos.
     */    
    public static void mostrarMenuDeCabeceraDeVentas()
    {
        System.out.println("\t\t*****************************************");
        System.out.println("\t\t*            MENU DE VENTAS             *");
        System.out.println("\t\t*****************************************");    
        mostrarMenuDeArticulos();
    }

    /**
     * Muestra el menu de cabecera de inventario con los articulos.
     */    
    public static void mostrarCabeceraDeInventario()
    {
        System.out.println("\t\t*****************************************************");
        System.out.println("\t\t*               I N V E N T A R I O                 *");
        System.out.println("\t\t*****************************************************");         
        mostrarCabeceraDeArticulos();
    }        
    
    /**
     * Muestra la cabecera de los articulos con el nombre, la cantidad, precio unitario y total.
     */
    public static void mostrarCabeceraDeArticulos()
    {
        System.out.println("\t\t-----------------------------------------------------");
        System.out.println("\t\t* Producto\t| Cantidad\t| Vr Unitario\t| Vr Total\t *");
        System.out.println("\t\t-----------------------------------------------------");        
    }        

    /**
     * Muestra el menu de cabecera de compras con los articulos.
     */        
    public static void mostrarCabeceraDeListaDeCompras()
    {
        System.out.println("\t\t*****************************************************");
        System.out.println("\t\t*                  C O M P R A S                    *");
        System.out.println("\t\t*****************************************************");         
        mostrarCabeceraDeArticulos();        
    } 
    
    
     /**
     * Muestra el menu de cabecera de ventas con los articulos.
     */    
    public static void mostrarCabeceraDeListaDeVentas()
    {
        System.out.println("\t\t*****************************************************");
        System.out.println("\t\t*                   V E N T A S                     *");
        System.out.println("\t\t*****************************************************");         
        mostrarCabeceraDeArticulos();        
    }     
    
    /**
     * El submenu de articulos.
     */
    public static void mostrarMenuDeArticulos()
    {
        System.out.println("\t\t\t1. Papas");
        System.out.println("\t\t\t2. Arroz");
        System.out.println("\t\t\t3. Carne");
        System.out.println("\t\t\t4. Jabon");
        System.out.println("\t\t\t5. Volver");
    }        
    
    /**
     * Muestra un mensaje indicando que se ingreso una opcion invalida.
     */
    public static void mostrarMensajeOpcionInvalida()
    {
        System.out.println("Ingreso una opcion invalida.");
    }    

    /**
     * Muestra el mensaje de salida del programa.
     */
    public static void mostrarMensajeSalida()
    {
        System.out.println("Gracias por usar nuestro programa.");
    }        
    
    /**
     * Muestra el mensaje de retorno a un menu anterior.
     */
    public static void mostrarMensajeRegresar(String nombre)
    {
        System.out.println("\t\t\tSaliendo del módulo " + nombre + ".");
    }      
    
    /**
     * Lista los productos por consola.
     * @param productos 
     */
    public static void mostrarStockDeArticulos(Producto[] productos)
    {
        for(Producto p: productos)
        {
            System.out.println("\t\t*| " + p.getNombre() + "\t| " + p.getCantidad() + "\t\t| " + p.getPrecio()+"\t\t| " + p.getCantidad() * p.getPrecio() +"\t\t*" );
        }    
    }       
    
    /**
     * Muestra un mensaje de espera.
     */
    public static void mostrarMensajeEspera()
    {
        System.out.print("\n\t\tDigite cualquier numero para continuar: ");
    }        
    
    /**
     * Muestra un mensaje de cantidad invalida.
     */
    public static void mostrarMensajeCantidadInsuficiente()
    {
        System.out.println("\t\tLa cantidad no es valida.\n");
    }        
    
    /**
     * Se encarga de listar la lista pasada por parametros. 
     * El parametro ganancia se usara con el valor 1.0 para las compras para no afectar el resultado.
     * Mientras que en las ventas puede tomar valores de 1.10 , 1.25, 1.50, etc. Segun la ganancia que
     * se quiera obtener por la venta.
     * @param listado
     * @param ganancia 
     */
    public static void mostrarListadoProductos(List<Producto> listado, Double ganancia)
    {
        for(Producto p : listado)
        {
            System.out.println("\t\t*| " + p.getNombre() + "\t| " + p.getCantidad() + "\t\t| " + p.getPrecio()+"\t\t| " + p.getCantidad() * p.getPrecio() * ganancia + "\t\t*");
        }    
    }
    
    /* Metodos alternativos para mostrar las compras y ventas.
    public static void mostrarComprasRealizadas(List<Producto> listado)
    {
        for(Producto p : listado)
        {
            System.out.println("\t\t*| " + p.getNombre() + "\t| " + p.getCantidad() + "\t\t| " + p.getPrecio()+"\t\t| " + p.getCantidad() * p.getPrecio() +"\t\t*");
        }    
    }
    
    public static void mostrarVentasRealizadas(List<Producto> listado)
    {
        for(Producto p : listado)
        {
            System.out.println("\t\t*| " + p.getNombre() + "\t| " + p.getCantidad() + "\t\t| " + p.getPrecio()+"\t\t| " + p.getCantidad() * p.getPrecio() * 1.25 +"\t\t*");
        }         
    } 
    */
}
