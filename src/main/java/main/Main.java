package main;

import controladores.Registradora;

/**
 *
 * @author oscar
 */
public class Main
{
    public static void main(String[] args)
    {
        Registradora registradora = new Registradora();
        registradora.registrar();
    }        
}
