package objetos;

/**
 *
 * @author oscar
 */
public abstract class Producto implements Cloneable
{
    private String nombre;
    private Integer cantidad;
    private Double precio;
    
    public Producto(){}
    
    public Producto(String nombre)
    {
        this.nombre = nombre;
    }
    
    public Producto(String nombre, Integer cantidad, Double precio)
    {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the cantidad
     */
    public Integer getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(Integer cantidad) 
    {
        if(cantidad < 0)
        {
            System.out.println("No puede asignar una cantidad negativa.");
            return;
        } 
        this.cantidad = cantidad;
    }

    /**
     * @return the precio
     */
    public Double getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(Double precio) {
        if(precio < 0)
        {
            System.out.println("No puede asignar un precio negativo.");
            return;
        }    
        this.precio = precio;
    }
    
    @Override
    public String toString()
    {
        return this.getNombre() + " - " + this.getCantidad() + " - " + this.getPrecio() ;
    }        
    
    /**
     * Metodo usado para clonar productos (Objetos).
     * Siempre hay que castear el tipo de objeto.
     * @return 
     */
    @Override
    public Producto clone() throws CloneNotSupportedException
    {
        return (Producto) super.clone();
    }        
    
}
