/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controladores;

import almacenamiento.BaseDeDatos;
import java.util.List;
import objetos.Producto;
import productos.*;
import vistas.Vista;

/**
 *
 * @author oscar
 */
public class Registradora 
{
    private BaseDeDatos baseDeDatos;
    public Registradora()
    {
        baseDeDatos = new BaseDeDatos();
    }
    
    /**
     * Se encarga de iniciar el proceso de la registradora.
     */
    public void registrar()
    {
        Integer opcion;
        do
        {    
            Vista.mostrarCabeceraPrincipal();
            Vista.mostrarMenuPrincipal();
            Vista.mostrarObtenerOpcion();
            opcion = Vista.obtenerOpcionPorTeclado();
            switch(opcion)
            {
                case 1:
                    comprar();
                    break;        
                case 2:
                    vender();
                    break; 
                case 3:
                    mostrarStock();
                    break; 
                case 4:
                    mostrarCompras();
                    break;
                case 5:
                    mostrarVentas();
                    break; 
                case 6:
                    Vista.mostrarMensajeSalida();
                    //Para salir del sistema.
                    System.exit(0);
                    break;
                default:
                    Vista.mostrarMensajeOpcionInvalida();
            }  
        } while(opcion >= 1 && opcion <= 6);          
    }
    
    /**
     * Se encarga de mostrar las opciones del menu de comprar y realizar la operación.
     */
    private void comprar()
    {
        Integer opcion;
        Vista.mostrarMenuDeCabeceraDeCompras();
        do
        {
            Vista.mostrarObtenerOpcion();
            opcion = Vista.obtenerOpcionPorTeclado();
            if(opcion >= 1 && opcion <= 4 )
            {
                comprarProducto(opcion);   
            } else if (opcion == 5){
                Vista.mostrarMensajeRegresar("Compras");
                return;
            } else {
                Vista.mostrarMensajeOpcionInvalida();
            }
        }while( opcion < 1 || opcion > 5 ) ;   
        
    }
    
    
    /**
     * Registra en la base de datos el producto comprado.
     * @param opcion 
     */
    private void comprarProducto(Integer opcion)
    {
        Integer cantidad;
        Double precio;
        Producto producto = null;
        switch(opcion)
        {
            case 1:
                producto = new Papa("Papa blanca");
                break;
            case 2:
                producto = new Arroz("Arroz fino");
                break;
            case 3:
                producto = new Carne("Pulpa");
                break;
            case 4:
                producto = new Jabon("Blanco Ala");
                break;
            default:
                Vista.mostrarMensajeOpcionInvalida();
        }    
        Vista.mostrarObtenerCantidad();
        cantidad = Vista.obtenerCantidadPorTeclado();
        Vista.mostrarObtenerPrecio();
        precio = Vista.obtenerPrecioPorTeclado();
        //Asignando el precio y la cantidad al producto.
        producto.setCantidad(cantidad);
        producto.setPrecio(precio);
        baseDeDatos.comprar(producto);
    }        
    
    /**
     * Se encarga de mostrar el stock de los productos almacenados.
     */
    private void mostrarStock()
    {
        Vista.mostrarCabeceraDeInventario();
        Vista.mostrarStockDeArticulos(baseDeDatos.obtenerTodos());
        Vista.mostrarMensajeEspera();
        Vista.obtenerOpcionPorTeclado();
    }

    /**
     * Se encarga de mostrar las opciones del menu de ventas y realizar la operación.
     */
    private void vender()
    {
        Integer opcion;
        Vista.mostrarMenuDeCabeceraDeVentas();
        do
        {
            Vista.mostrarObtenerOpcion();
            opcion = Vista.obtenerOpcionPorTeclado();
            if(opcion >= 1 && opcion <= 4 )
            {
                venderProducto(opcion);   
            } else if (opcion == 5){
                Vista.mostrarMensajeRegresar("Ventas");
                return;
            } else {
                Vista.mostrarMensajeOpcionInvalida();
            }
        }while( opcion < 1 || opcion > 5 ) ;   
    }
    
    /**
     * Registra en la base de datos el producto vendido.
     * @param opcion 
     */
    private void venderProducto(Integer opcion)
    {
        Integer cantidad;
        Double precio;
        Producto producto = null;
        if(opcion < 1 || opcion > 4)
        {
            Vista.mostrarMensajeOpcionInvalida();
            return;
        }    
        producto = baseDeDatos.obtenerPorIndice(opcion-1); //Se obtiene el producto a vender.
        //Se pide la cantidad y luego se valida.
        Vista.mostrarObtenerCantidad();
        cantidad = Vista.obtenerCantidadPorTeclado();
        if( producto.getCantidad() < cantidad )
        {
            Vista.mostrarMensajeCantidadInsuficiente();
            return;
        }    
        producto.setCantidad(cantidad);
        baseDeDatos.vender(producto);
    }       
    
    /**
     * Se encarga de mostrar el listado de compras.
     */
    private void mostrarCompras()
    {
        List<Producto> listadoCompras = baseDeDatos.obtenerCompras();
        Vista.mostrarCabeceraDeListaDeCompras();
        Vista.mostrarListadoProductos(listadoCompras, 1.0); // Aqui la ganancia es 1.0 para no afectar el resultado.
        Vista.mostrarMensajeEspera();
        Vista.obtenerOpcionPorTeclado();
    }   
    
    /**
     * Se encarga de mostrar el listado de ventas.
     */
    private void mostrarVentas()
    {
         List<Producto> listadoVentas = baseDeDatos.obtenerVentas();
         Vista.mostrarCabeceraDeListaDeVentas();
         Vista.mostrarListadoProductos(listadoVentas, 1.25); //Aqui es 1.25 oara reflejar una ganancia de 25% .
         Vista.mostrarMensajeEspera();
         Vista.obtenerOpcionPorTeclado();
    }        
    
    
}
