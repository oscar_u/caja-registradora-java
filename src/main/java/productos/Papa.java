/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package productos;

import objetos.Producto;

/**
 *
 * @author oscar
 */
public class Papa extends Producto
{
    public Papa(){}
    
    public Papa(String nombre)
    {
         super(nombre);
    }
    public Papa(String nombre, Integer cantidad, Double precio)
    {
         super(nombre, cantidad, precio);
    }
}
