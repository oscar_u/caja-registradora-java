package productos;

import objetos.Producto;

/**
 *
 * @author oscar
 */
public class Jabon extends Producto
{
    public Jabon(){}
    
    public Jabon(String nombre)
    {
        super(nombre);
    }
    public Jabon(String nombre, Integer cantidad, Double precio)
    {
         super(nombre, cantidad, precio);
    }
}
