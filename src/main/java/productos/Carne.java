/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package productos;

import objetos.Producto;

/**
 *
 * @author oscar
 */
public class Carne extends Producto
{
    public Carne(){}
    public Carne(String nombre)
    {
        super(nombre);
    }
    public Carne(String nombre, Integer cantidad, Double precio)
    {
         super(nombre, cantidad, precio);
    }
}
