/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package almacenamiento;

import java.util.ArrayList;
import java.util.List;
import objetos.Producto;
import productos.*;

/**
 *
 * @author oscar
 */
public class BaseDeDatos 
{
    private Producto[] productos;
    private List<Producto> compras;
    private List<Producto> ventas;    
    
    /**
     * Este constructor al crear la base de datos instacia 4 productos.
     */
    public BaseDeDatos()
    {
        this.compras = new ArrayList<>();
        this.ventas = new ArrayList<>();
        productos = new Producto[4];
        Producto papa = new Papa("Papa blanca",0,0.0);
        Producto arroz = new Arroz("Arroz fino",0,0.0);
        Producto carne = new Carne("Pulpa",0,0.0);
        Producto jabon = new Jabon("Blanco Ala",0,0.0);
        
        productos[0] = papa;
        productos[1] = arroz;
        productos[2] = carne;
        productos[3] = jabon;           
    }        
    
    /**
     * Recibe como parametro un indice y retorna el producto correspondiente
     * en el arreglo de productos.
     * @param i
     * @return 
     */
    public Producto obtenerPorIndice(int i)
    {
        Producto productoObtenido = null;
        if( i < 0 || i > this.productos.length - 1 )
        {
            System.out.println("almacenamiento.BaseDeDatos.obtenerPorIndice()");
            return null;
        }
        //Aqui es donde se clona el producto. Puede dar error por eso la Exception.
        try
        {
            productoObtenido  = productos[i].clone();
        }catch(CloneNotSupportedException cnse)
        {
            System.out.println(cnse);
        }    
        return productoObtenido ;
    }        
    
    /**
     * Devuelve todo el arreglo de objetos del tipo producto.
     * @return 
     */
    public Producto[] obtenerTodos()
    {
        Producto[] productosObtenidos = new Producto[4];
        try {
            for(int i = 0 ; i < this.productos.length ; i++ )
            {
                productosObtenidos[i] = productos[i].clone();
            }    
        } catch (CloneNotSupportedException cnse) 
        {
            System.out.println(cnse);
        }
        return productosObtenidos;
    }        
    
    
    /**
     * Metodo encargado de registrar las compras.
     * @param productoComprado 
     */
    public void comprar(Producto productoComprado)
    {
        Producto temporal;
        switch (productoComprado.getClass().getSimpleName()) {
            case "Papa":
                temporal = this.productos[0];
                break;
            case "Arroz":
                temporal = this.productos[1];
                break;
            case "Carne":
                temporal = this.productos[2];
                break;
            case "Jabon":
                temporal = this.productos[3];
                break;                
            default:
               System.out.println("Producto invalido.");
                return;
        }
        temporal.setCantidad(temporal.getCantidad() + productoComprado.getCantidad());
        temporal.setPrecio(productoComprado.getPrecio());
        this.compras.add(productoComprado);  
    }
    
    /**
     * Metodo encargado de registrar las ventas.
     * @param productoVendido 
     */
    public void vender(Producto productoVendido)
    {
        Producto temporal;
        switch (productoVendido.getClass().getSimpleName()) {
            case "Papa":
                temporal = this.productos[0];
                break;
            case "Arroz":
                temporal = this.productos[1];
                break;
            case "Carne":
                temporal = this.productos[2];
                break;
            case "Jabon":
                temporal = this.productos[3];
                break;                
            default:
               System.out.println("Producto invalido.");
                return;
        }
        temporal.setCantidad(temporal.getCantidad() - productoVendido.getCantidad());
        productoVendido.setPrecio(temporal.getPrecio() * 1.25);
        this.ventas.add(productoVendido);  
    }
    
    public List<Producto> obtenerCompras()
    {
        return this.compras;
    }        
 
    public List<Producto> obtenerVentas()
    {
        return this.ventas;
    }       
}
